# Classificacion de lenguajes segun su generación

## Introducción

Los lenguajes de programacion han sido desarrollados por años y han pasado varias etapas
y por cad etapa ha hecho los lenguajes mas amigables al usuario y mas alejados del codigo binario
y en general mas facil de usar y con cada etapa que ha mejorado los lenguajes se le llama generación.
Todas las etapas se pueden classificar en 5 generaciones diferentes.

| Generación      |    Capa    |
| :-------------: |-------------|
|  1r Generación  |   Lenguaje maquina  |
|  2n Generación     | Lenguaje assemblador     |
| 3r Generación     |  Lenguajes de alto nivel     |
| 4t Generación      | Lenguaje de muy alto nivel     |
| 5t Generación      | Lenguaje orientado a objetos con usa de IDEs    |


## Historia
 
### Primer lenguaje
 
El primer lenguaje de programación de alto nivel para un ordenador fue Plankalkül hecho por Konrad Zuse para utilizarlo para la ingeniería entre 1942 y 1945.
 
 
 
### Generaciones
 
1. Los programadores en los años 50 utilizaban el lenguaje de máquinas.
 
2. En poco tiempo salió la segunda generación conocida como lenguaje ensamblador.
 
3. Aun en los años 40 el lenguaje de ensamblador fue reemplazado por la tercera generación que son más abstractos y se pueden implementar de forma a los ordenadores con un código diferente al código de la máquina nativa.
 
4. Al final de los años 50 se introdujo ALGOL 60, que la gran parte de los lenguajes descienden de él .



### 1ª generación
 
És el más básico, pero en realidad el único que comprende el ordenador. Se centra en nombres en código binario, la resta de lenguajes actuan traduciendo el código. Son a los que corresponden los lenguajes ya vistos de [bajo nivel o lenguajes de máquina](https://es.wikipedia.org/wiki/Lenguaje_de_bajo_nivel).

### Ejemplo Binario
 
 ```
1100000011110000000000000011110100001001000001010000100100001001000001111110100000000000010100010000111111100100000000000000000001000000111110001101000001100011010010100000100101000111001000111111111111101000100010100000010100111001000000
```


### 2ª generación
 
Simplifican la escritura de las instrucciones y las hacen más legibles. Son aquellos que pertenecen a los lenguajes [ensambladores](https://es.wikipedia.org/wiki/Ensamblador).

#### Ejemplo de Assembler

```
*
* Sample of a callable program (non-reusable or reusable)
* This sample is not usable for re-enterable or refreshable programs.
*
SUBPROG  CSECT
         USING SUBPROG,R15         Register 15 contains address
SUBPROG
         B     START               Skip data
         DC    C'SUBPROG '         Program-name
         DC    C'&SYSDATE'         Date
         DC    C'&SYSTIME'         Time
         DC    C'V1R2.05'          Version number
         DS    0H                  Re-align on halfword-boundary
```


### 3ª Generación
 
En la tercera generación pertenecen aquellos lenguajes estructurados que seguían un orden a la hora de ejecutar las instrucciones. Lenguajes como **[C](https://es.wikipedia.org/wiki/C_(lenguaje_de_programaci%C3%B3n) "C (lenguaje de programación)
"), [pascal](https://es.wikipedia.org/wiki/Pascal_(lenguaje_de_programaci%C3%B3n) "Pascal (lenguaje de programación)
"), [cobol](https://ca.wikipedia.org/wiki/COBOL "COBOL"), etc.**
#### Ejemplo c
 
```c
/* Programa: Hola mundo */
 
#include <conio.h>
#include <stdio.h>
 
int main()
{
   printf( "Hola mundo." );
 
   getch(); /* Pausa */
 
   return 0;
}
```
 
![alt text](https://images.vexels.com/media/users/3/166179/isolated/lists/b83d6b47a9502dfaf535087627a8bf96-icono-del-lenguaje-de-programacion-c.png)
 
### 4ª Generación
 
A partir de una serie de predicados y una base de conocimiento se desarrolla este paradigma de relaciones lógicas. Ejemplo de este paradigma es el lenguaje de programación [Prolog](https://es.wikipedia.org/wiki/Prolog). Los lenguajes de cuarta generación, son los más cercanos a la sintaxis de la lengua humana, y se suelen utilizar en las creaciones de bases de datos o como lenguajes de programación de los lenguajes o sistemas de autor. Son lenguajes no procedimentales como [SQL](https://es.wikipedia.org/wiki/SQL) que permiten definir cuáles serán los resultados finales sin necesidad de preocuparse por saber cómo hacerlo.
 
#### exemple python
 
```python
def suma(a, b):
 return a + b
 
result = suma(1, 2)
# result = 3
```
 
![alt text](https://developer.asustor.com/uploadIcons/0020_999_1596451274_python.png)
 
 
### 5ª Generación
 
Con la incorporación y expansión de los lenguajes orientados a objetos y con la generalización del uso de las [GUI](https://es.wikipedia.org/wiki/Interfaz_gr%C3%A1fica_de_usuario "Interfaz gráfica de usuario"), es posible abarca problemas de mayor abstracción. Algunos ejemplos son [Java](https://ca.wikipedia.org/wiki/Java_(llenguatge_de_programaci%C3%B3)), [C ++](https://es.wikipedia.org/wiki/C%2B%2B), etc. Se ha generalizado tanto los [IDEs](https://es.wikipedia.org/wiki/Entorno_de_desarrollo_integrado "Entorno de desarrollo integrado") de desarrollo como los clisés para cada servicio / lenguaje.
 
#### Ejemplo Java
 
```java
public class Nombre_Clase
{
    public static void main (String args[])
    {
       //bloque de sentencias;
    }
}
```
 
 
![alt text](https://i.blogs.es/6091fa/java/450_1000.jpg)
 
